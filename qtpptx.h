#ifndef QTPPTX_H
#define QTPPTX_H

#include <QObject>
#include <QAxObject>
#include <shlobj.h>
#include "ptxslides.h"

class QtPptx : public QObject
{
    Q_OBJECT
public:
    QtPptx();
    void openPptx(QString path);
    ptxslides *slides;
    QAxObject *presentation;
    void setVisbility(bool visible);
    void close();
    void closePresentation();

private:
    QAxObject *powerPoint;
    QAxObject *presentations;
};

#endif // QTPPTX_H
