#include "qtpptx.h"
#include <QDebug>

QtPptx::QtPptx()
{
    CoInitializeEx(NULL, COINIT_MULTITHREADED);
    powerPoint = new QAxObject("PowerPoint.Application", 0);
    presentations = powerPoint->querySubObject("Presentations");
}

void QtPptx::openPptx(QString path)
{
    presentation = presentations->querySubObject("Open(QString)", path.replace("/","\\\\"));
    if (!presentation)
        presentation = presentations->querySubObject("Open(QString)", path);
    slides = (ptxslides *)presentation->querySubObject("Slides");
}

void QtPptx::setVisbility(bool visible)
{
    powerPoint->setProperty("Visible", visible);
}

void QtPptx::close()
{
    powerPoint->dynamicCall("Quit()");
    presentations->deleteLater();
    powerPoint->deleteLater();
}

void QtPptx::closePresentation()
{
    presentation->dynamicCall("Close()");
}

