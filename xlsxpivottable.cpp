#include "xlsxpivottable.h"
#include <QDebug>
#include <QTextEdit>

xlsxPivotTable::xlsxPivotTable()
{

}

QString xlsxPivotTable::getPivotName()
{
    return this->property("Name").toString();
}

void xlsxPivotTable::addField(QString fieldName, xlsxFieldType fieldType)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    pivotField->setProperty("Orientation", fieldType);
    pivotField->deleteLater();
}

void xlsxPivotTable::addDataField(QString fieldName, QString caption, xlsxPivotTable::XlConsolidationFunction function)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", fieldName);
    this->dynamicCall("AddDataField(QVariant, QString, XlConsolidationFunction)", pivotField->asVariant(), caption, function);
    pivotField->deleteLater();
}

QPoint xlsxPivotTable::getTopLeftCell()
{
    QAxObject *rowRange = this->querySubObject("RowRange");
    QPoint result;
    result.setX(rowRange->dynamicCall("Row").toInt());
    result.setY(rowRange->dynamicCall("Column").toInt());
    return result;
}

QRect xlsxPivotTable::getDataRange()
{
    QPoint startPoint = getTopLeftCell();
    QSize endPoint; //QPoint делает вычетание высоты при объединении QPoint1 & QPoint2, а QSize - нет

    QAxObject *rowRange = this->querySubObject("RowRange");
    QAxObject *columnsRange = this->querySubObject("DataBodyRange");
    QAxObject *columns = columnsRange->querySubObject("Columns");

//    qDebug() << startPoint.x();
//    qDebug() << startPoint.y();

//    qDebug() << rowRange->dynamicCall("Count").toInt();
//    qDebug() << columns->dynamicCall("Count").toInt();


    endPoint.setHeight(rowRange->dynamicCall("Count").toInt() + startPoint.x() - 1); //Т.к. StartPoint уже содержит первую ячейку, значит нужно делать смещение на -1
    endPoint.setWidth(columns->dynamicCall("Count").toInt() + startPoint.y()); //Данный метод не захватывает первую ячейку, так что не надо делать смещение

//    qDebug() << endPoint;

    return QRect(startPoint, endPoint);
}

void xlsxPivotTable::setSingleFilter(QString field, QString filter)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", field);
    pivotField->dynamicCall("ClearAllFilters()");
    pivotField->dynamicCall("SetCurrentPage(QString)", filter);
}

void xlsxPivotTable::setMultipleFilter(QString field, QStringList filters)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", field);
    int count = pivotField->querySubObject("PivotItems")->property("Count").toInt();

    for (int i = 1; i <= count; i++) //подсчет начинается с 1
    {
        QAxObject *pivotItem = pivotField->querySubObject("PivotItems(int)", i);
        if (filters.contains( pivotItem->property("Name").toString() ))
            pivotItem->setProperty("Visible", true);
        else
            pivotItem->setProperty("Visible", false);
    }
}

void xlsxPivotTable::setCalculationType(QString field, xlsxPivotTable::XlPivotFieldCalculation type)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", field);
    pivotField->setProperty("Calculation", type);
}

void xlsxPivotTable::setSort(QString sortedField, QString sortByField, xlsxPivotTable::XlSortOrder order)
{
    QAxObject *pivotField = this->querySubObject("PivotFields(QString)", sortedField);
    pivotField->dynamicCall("AutoSort(XlSortOrder, QString)", order, sortByField);
}

void xlsxPivotTable::setTotalGrand(bool boolean)
{
    this->setProperty("ColumnGrand", boolean);
    this->setProperty("RowGrand", boolean);
}

void xlsxPivotTable::setRowGrand(bool boolean)
{
    this->setProperty("RowGrand", boolean);
}

void xlsxPivotTable::setColumnGrand(bool boolean)
{
    this->setProperty("ColumnGrand", boolean);
}

void xlsxPivotTable::setRowAxisLayout(xlsxPivotTable::XlLayoutRowType type)
{
    this->setProperty("RowAxisLayout", type);
}

void xlsxPivotTable::clearAllFields()
{
    QAxObject *pivotFields = this->querySubObject("PivotFields");
    int count = pivotFields->property("Count").toInt();
    for (int i = 1; i <= count; i++)
        pivotFields->querySubObject("Item(int)", i)->setProperty("Orientation", xlHidden);

    QAxObject *dataFields = this->querySubObject("DataFields");
    count = dataFields->property("Count").toInt();
    for (int i = 1; i <= count; i++)
        dataFields->querySubObject("Item(int)", i)->setProperty("Orientation", xlHidden);
}

