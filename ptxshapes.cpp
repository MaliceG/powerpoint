#include "ptxshapes.h"
#include <QDebug>
#include <QTextEdit>

ptxshapes::ptxshapes()
{

}

int ptxshapes::getCount()
{
    return this->property("Count").toInt();
}

ptxShape *ptxshapes::shape(int index)
{
    return (ptxShape*)this->querySubObject("item(int)", index);
}

ptxShape *ptxshapes::shape(QString name)
{
    return (ptxShape*)this->querySubObject("item(QString)", name);
}

ptxShape *ptxshapes::addChart2(ptxshapes::chartType type, int x1, int y1, int x2, int y2)
{
    qDebug() << "Return ptxShape";
    return (ptxShape*)this->querySubObject("AddChart2(int, Office::XlChartType , int, int, int, int)", -1, type, x1, y1, x2, y2);
}

ptxShape *ptxshapes::addChart(ptxshapes::chartType type, int x1, int y1, int x2, int y2)
{
    return (ptxShape*) this->querySubObject("AddChart(Office::XlChartType , int, int, int, int)", type, x1, y1, x2, y2);
}

void ptxshapes::pasteWithFormat()
{
    this->dynamicCall("PasteSpecial(ppPasteEnhancedMetafile)");
}

void ptxshapes::paste()
{
    this->dynamicCall("Paste()");
}

