#include "ptxslides.h"
#include <QDebug>

ptxslides::ptxslides()
{

}

int ptxslides::getCount()
{
    return this->property("Count").toInt();
}

ptxslide *ptxslides::slide(int index)
{
    return (ptxslide*)this->querySubObject("Item(int)", index);
}

void ptxslides::addSlide(int index)
{
    this->dynamicCall("Add(int, PpSlideLayout)", index, "ppLayoutCustom");
}

