#ifndef XLSXPIVOTTABLE_H
#define XLSXPIVOTTABLE_H

#include <QObject>
#include <QAxObject>
#include <QPoint>
#include <QRect>

class xlsxPivotTable : public QAxObject
{
public:

    enum xlsxFieldType{
        xlHidden = 0,
        xlRowField = 1,
        xlColumnField = 2,
        xlPageField = 3,
        xlDataField = 4
    };

    enum XlConsolidationFunction{
        xlSum = -4157
    };

    enum XlPivotFieldCalculation{
        xlPercentOfColumn = 7,
        xlPercentOfRow = 6
    };

    enum XlSortOrder{
        xlAscending = 1,
        xlDescending = 2
    };

    enum XlLayoutRowType{
        xlDefault = 0,
        xlCompactRow = 0,
        xlTabularRow = 1,
        xlOutlineRow = 2
    };

    xlsxPivotTable();
    QString getPivotName();

    void addField(QString fieldName, xlsxFieldType fieldType);
    void addDataField(QString fieldName, QString caption, XlConsolidationFunction function);
    QPoint getTopLeftCell();
    QRect getDataRange();
    void setSingleFilter(QString field, QString filter);
    void setMultipleFilter(QString field, QStringList filters);
    void setCalculationType(QString field, XlPivotFieldCalculation type);
    void setSort(QString sortedField, QString sortByField, XlSortOrder order);
    void setTotalGrand(bool boolean);
    void setRowGrand(bool boolean);
    void setColumnGrand(bool boolean);
    void setRowAxisLayout(XlLayoutRowType type);
    void clearAllFields();
};

#endif // XLSXPIVOTTABLE_H
