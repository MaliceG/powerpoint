#ifndef XLSXWORKBOOK_H
#define XLSXWORKBOOK_H

#include <QObject>
#include <QAxObject>
#include "xlsxworksheet.h"

class xlsxWorkbook : public QAxObject
{
public:
    xlsxWorkbook();
    xlsxWorksheet *openSheet(int index);
    xlsxWorksheet *openSheet(QString sheetName);
    int getSheetsCount();
    void addWorksheet(QString name);
    void close(bool save);
    bool isWorksheetExist(QString name);
};

#endif // XLSXWORKBOOK_H
