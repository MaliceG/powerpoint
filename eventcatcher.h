#ifndef EVENTCATCHER_H
#define EVENTCATCHER_H

#include <QObject>

class eventCatcher : public QObject
{
    Q_OBJECT
public:
    explicit eventCatcher(QObject *parent = 0);

signals:
    windowActivated();
    WorkbookDeactivated();

public slots:
    void newWorkbook();
    void SheetActivate();
    void WindowActivate();
    void WindowDeactivate();
    void WorkbookActivate();
    void WorkbookDeactivate();
    void WorkbookOpen();
};

#endif // EVENTCATCHER_H
