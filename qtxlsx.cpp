#include "qtxlsx.h"

qtXlsx::qtXlsx()
{
    CoInitializeEx(NULL, COINIT_MULTITHREADED);
    excel = new QAxObject("Excel.Application", 0);
    excel->setProperty("Visible", true);
    books = excel->querySubObject("Workbooks");
}

void qtXlsx::openWorkbook(QString filePath)
{
    workbook = (xlsxWorkbook*) books->querySubObject("Open(const QString&)", filePath.replace("/","\\\\"));
    if (!workbook)
        workbook = (xlsxWorkbook*) books->querySubObject("Open(const QString&)", filePath);
}

void qtXlsx::closeWorkbooks()
{
    books->dynamicCall("Close()");
    excel->dynamicCall("Quit()");
    delete books;
    delete excel;
}

void qtXlsx::setScreenUpdates(bool scr)
{
    excel->setProperty("ScreenUpdating", scr);
}

void qtXlsx::setVisible(bool visible)
{
    excel->setProperty("Visible", visible);
}

