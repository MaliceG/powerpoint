#include "xlsxpivottables.h"
#include <QDebug>

xlsxPivotTables::xlsxPivotTables()
{

}

xlsxPivotTable *xlsxPivotTables::initPivot(int index)
{
//    if (getCount() > 0)
    activePivot = (xlsxPivotTable*) this->querySubObject("Item(int)", index);
    return activePivot;
}

int xlsxPivotTables::getCount()
{
    return this->dynamicCall("Count()").toInt();
}

