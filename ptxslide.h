#ifndef PTXSLIDE_H
#define PTXSLIDE_H
#include <QAxObject>
#include <QObject>
#include "ptxshapes.h"

class ptxslide : public QAxObject
{
public:
    ptxslide();
    ptxshapes *shapes();
};

#endif // SLIDE_H
