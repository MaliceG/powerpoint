#include "xlsxworkbook.h"
#include <QDebug>

xlsxWorkbook::xlsxWorkbook()
{

}

xlsxWorksheet *xlsxWorkbook::openSheet(int index)
{
    QAxObject *sheets = this->querySubObject("Worksheets");
    return (xlsxWorksheet*) sheets->querySubObject("Item(int)", index);
}

xlsxWorksheet *xlsxWorkbook::openSheet(QString sheetName)
{
    return (xlsxWorksheet*) this->querySubObject("Worksheets(const QString&)", sheetName);
}

int xlsxWorkbook::getSheetsCount()
{
    QAxObject *sheets = this->querySubObject("Worksheets");
    return sheets->property("Count").toInt();
}

void xlsxWorkbook::addWorksheet(QString name)
{
    QAxObject *worksheets = this->querySubObject("Worksheets");
    QAxObject *sheet = worksheets->querySubObject("Add()");
    sheet->setProperty("Name", name);
}

void xlsxWorkbook::close(bool save)
{
    this->dynamicCall("Close(QVariant)", save);
}

bool xlsxWorkbook::isWorksheetExist(QString name)
{
    QAxObject *worksheets = this->querySubObject("Worksheets");
    int worksheetsCount = worksheets->property("Count").toInt();
    qDebug() << "COUNT:" << worksheetsCount;
    for (int i = 1; i <= worksheetsCount; i++)
    {
        if (name == worksheets->querySubObject("Item(int)", i)->property("Name"))
        {
            delete worksheets;
            return true;
        }
    }
    delete worksheets;
    return false;
}

