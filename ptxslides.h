#ifndef PTXSLIDES_H
#define PTXSLIDES_H
#include <QAxObject>
#include "ptxslide.h"
#include <QObject>


class ptxslides : public QAxObject
{   
    Q_OBJECT
public:
    explicit ptxslides();
    int getCount();
    ptxslide *slide(int index);
    void addSlide(int index);


};

#endif // SLIDES_H
