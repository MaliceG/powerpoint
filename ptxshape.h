#ifndef PTXSHAPE_H
#define PTXSHAPE_H

#include <QObject>
#include <QAxObject>
#include "xlsxworksheet.h"
#include "xlsxworkbook.h"
#include <shlobj.h>
#include "eventcatcher.h"

class ptxShape : public QAxObject
{
    Q_OBJECT
private:
//    QAxObject *chart;
//    QAxObject *chartData;
//    QAxObject *workbook;
//    QAxObject *workbookApp;
    eventCatcher *ec;

public:
    ptxShape();

    void closeWorkbook(xlsxWorkbook *workbook);
    void setSourceData(xlsxWorksheet *wbSheet, int startRow, int startCol, int endRow, int endCol);
    void setWidth(int width);
    void setPos(QPoint pos);
    void remove();
    void setText(QString text);

    xlsxWorkbook* initWorkbook();
    xlsxWorksheet* openWorksheet(xlsxWorkbook *workbook, int index);

    QString getName();
    QString getText();
    QPoint getPos();

    int getHeight();
    int getWidht();
    int getType();

    bool isChart();

//    xlsxWorksheet *wbSheet;

public slots:
    void test();
};

#endif // PTXSHAPE_H
